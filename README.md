# README #

This repo contains source code of simple native iPhone application for management of personal notes. Application communicates to mock server. App's behaviour is based on server's responses. Server provides only static responses, thereby behavior of application might be sometimes confusing. 
For example endpoint dedicated to update note returns the same static response for each call regardless to the request. Thus, application UI might not reflect updated reality but it reflects to the server's responses.

### How do I get set up? ###

* Clone repo
* run `pod install`  in project directory
* build & run the code





