import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    private let dependencies = AppDependencies()
    var window: UIWindow?
    private var rootFlowCoordinator: RootFlowCoordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        let rootFlow = RootFlowCoordinator(dependencies: dependencies, window: window)
        rootFlow.start()
        return true
    }
}

