import UIKit

open class BaseTableViewController: BaseViewController {
    public weak var tableView: UITableView!
    public weak var refreshControl: UIRefreshControl!
    private let tableViewStyle: UITableView.Style

    // MARK: Initializers

    public init(tableViewStyle: UITableView.Style = .grouped) {
        self.tableViewStyle = tableViewStyle
        super.init()
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: View life cycle

    override open func loadView() {
        super.loadView()

        let tableView = UITableView(frame: .zero, style: tableViewStyle)
        tableView.backgroundColor = .clear
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalTo(view.safeAreaLayoutGuide)
        }
        self.tableView = tableView
        
        let refreshControl = UIRefreshControl()
        self.refreshControl = refreshControl
        tableView.refreshControl = refreshControl
    }
}
