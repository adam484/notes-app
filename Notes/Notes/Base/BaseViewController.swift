import UIKit
import PKHUD

open class BaseViewController: UIViewController {
    public init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    /// Start or stop loading
    open var isLoading = false {
        didSet {
            if isLoading {
                PKHUD.sharedHUD.show()
            } else {
                PKHUD.sharedHUD.hide()
            }
        }
    }

    open override func loadView() {
        super.loadView()
        view.backgroundColor = UIColor.white
    }

    open override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }

    public required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
