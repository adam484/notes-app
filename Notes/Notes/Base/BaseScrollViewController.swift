import ReactiveSwift
import SnapKit
import UIKit

open class BaseScrollViewController: BaseViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate {
    /// Main whole screen scroll view
    public weak var scrollView: UIScrollView!

    /// Edge-to-edge content view for resolving content size.
    public weak var scrollContentView: UIView!

    /// Tap recognizer on self.view for dismissing keyboard
    public var tapGestureRecognizer: UITapGestureRecognizer!

  
    /// Decides if `scrollView`'s bottom should follow keyboard changes
    public var shouldObserveKeyboardChanges = true {
        didSet {
            guard oldValue != shouldObserveKeyboardChanges else { return }
            if shouldObserveKeyboardChanges {
                registerForKeyboardNotifications()
            } else {
                unregisterForKeyboardNotifications()
            }
        }
    }

    open override func loadView() {
        super.loadView()

        let scrollView = UIScrollView()
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { make in
            make.leading.trailing.top.equalTo(view.safeAreaInsets)
            make.bottom.equalTo(0)
        }
        self.scrollView = scrollView
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false

        let scrollContentView = UIView()
        scrollView.addSubview(scrollContentView)
        scrollContentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.width.equalTo(view.snp.width)
        }
        self.scrollContentView = scrollContentView

        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.cancelsTouchesInView = false
        tapGestureRecognizer.addTarget(self, action: #selector(viewTapped(sender:)))
        tapGestureRecognizer.isEnabled = false
        view.addGestureRecognizer(tapGestureRecognizer)
        self.tapGestureRecognizer = tapGestureRecognizer
        
    }
    
    @objc func viewTapped(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            self.view.endEditing(true)
        }
    }

    open override func viewDidLoad() {
        super.viewDidLoad()
        tapGestureRecognizer.delegate = self
        registerForKeyboardNotifications()
    }

    deinit {
        unregisterForKeyboardNotifications()
    }

    // MARK: - Keyboard handling

    @objc open func keyboardWillChangeFrame(_ notification: Notification) {
        guard let rectEnd = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect,
            let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double
        else { return }

        let screenHeight = UIScreen.main.bounds.size.height
        var bottom = screenHeight - rectEnd.origin.y

        if let tabBar = tabBarController?.tabBar, rectEnd.origin.y != UIScreen.main.bounds.size.height, tabBar.isHidden == false {
            bottom -= tabBar.frame.height
        }

        scrollView?.snp.updateConstraints { make in
            make.bottom.equalTo(-bottom)
        }

        UIView.animate(withDuration: duration) { [weak self] in
            self?.view.layoutIfNeeded()
        }
    }

    // MARK: Private helpers

    private func registerForKeyboardNotifications() {
        guard shouldObserveKeyboardChanges else { return }
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame(_:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }

    private func unregisterForKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
}
