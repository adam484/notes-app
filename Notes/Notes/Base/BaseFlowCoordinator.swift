import Foundation
import UIKit

public protocol FlowCoordinator: class {
    var childCoordinators: [BaseFlowCoordinator] { get set }

    var parent: BaseFlowCoordinator? { get set }
    
    func start()
    
    /// Get child by it's type
    func getChild<T>(_ typeOf: T.Type) -> T?

    /// Add child to current FlowCoordinator
    func addChild(_ flowController: BaseFlowCoordinator)

    /// Remove one concrete child
    func removeChild(_ flowController: BaseFlowCoordinator)
}


open class BaseFlowCoordinator: NSObject, FlowCoordinator {
    public var childCoordinators: [BaseFlowCoordinator] = [BaseFlowCoordinator]()
    
    public var parent: BaseFlowCoordinator?
    
    /// Reference to the navigation controller used within the flow
    public weak var navigationController: UINavigationController?

    public weak var rootViewController: UIViewController!
    
    public func getChild<T>(_ typeOf: T.Type) -> T? {
        return childCoordinators.first { (child) -> Bool in
            type(of: child) == T.self
        } as? T ?? nil
    }
    
    public func addChild(_ flowController: BaseFlowCoordinator) {
        if !childCoordinators.contains(flowController) {
            childCoordinators.append(flowController)
            flowController.parent = self
        }
    }
    
    public func removeChild(_ flowController: BaseFlowCoordinator) {
        if let index = childCoordinators.firstIndex(where: { $0 == flowController }) {
            childCoordinators.remove(at: index)
        }
    }

    open func start() {}
}
