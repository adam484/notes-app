import UIKit

open class BaseNavigationController: UINavigationController {
    public override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        modalPresentationStyle = .fullScreen
        super.pushViewController(viewController, animated: animated)
    }

    public override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        modalPresentationStyle = .fullScreen
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
}
