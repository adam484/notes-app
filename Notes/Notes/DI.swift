import Foundation


protocol HasNoDependency {}
typealias HasAllDependencies = HasNetwork & HasNoDependency & HasNotesAPI

class AppDependencies: HasAllDependencies {
    
    public lazy var network: Networking = Network(dependencies: self)
    public lazy var notesAPI: NotesAPIServicing = NotesAPI(dependencies: self)
}
