import Foundation
import ReactiveSwift

protocol HasNotesAPI {
    var notesAPI: NotesAPIServicing { get }
}

protocol NotesAPIServicing {
    func notes() -> SignalProducer<[Note], NSError>
    func deleteNote(id: IdentifierType) -> SignalProducer<Void, NSError>
    func updateNote(note: Note) -> SignalProducer<NoteUpdateResponse, NSError>
    func createNote(title: String) -> SignalProducer<Note, NSError>
}

final class NotesAPI: NotesAPIServicing {
    
    func notes() -> SignalProducer<[Note], NSError> {
        return network.requesta("/notes", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
    }
    
    func deleteNote(id: IdentifierType) -> SignalProducer<Void, NSError> {
        return network.request("/notes/\(id)", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: nil)
    }
    
    func updateNote(note: Note) -> SignalProducer<NoteUpdateResponse, NSError> {
        let params: [String: Any] = ["title" : note.title]
        return network.request("/notes/\(note.id)", method: .put, parameters: params, encoding: JSONEncoding.default, headers: nil)
    }
    
    func createNote(title: String) -> SignalProducer<Note, NSError> {
        let params: [String: Any] = ["title" : title]
        return network.request("/notes", method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
    }
    
    typealias Dependencies = HasNetwork
    private let network: Networking
    
    init(dependencies: Dependencies) {
        self.network = dependencies.network
    }
}
