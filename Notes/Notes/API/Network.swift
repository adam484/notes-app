import Foundation
import Alamofire
import ReactiveSwift


public protocol HasNetwork {
    var network: Networking { get }
}

public protocol Networking {
    var baseURL: URL { get set }
    func request(_ address: String, method: HTTPMethod, parameters: Parameters?,
                 encoding: ParameterEncoding, headers: HTTPHeaders?) -> SignalProducer<Void, NSError>
    func request<T: Decodable>(_ address: String, method: HTTPMethod, parameters: Parameters?,
                               encoding: ParameterEncoding, headers: HTTPHeaders?) -> SignalProducer<T, NSError>
    func requesta<T: Decodable>(_ address: String, method: HTTPMethod, parameters: Parameters?,
                               encoding: ParameterEncoding, headers: HTTPHeaders?) -> SignalProducer<[T], NSError>
}

public final class Network: SessionDelegate, Networking {
    public func request<T>(_ address: String, method: HTTPMethod, parameters: Parameters?, encoding: ParameterEncoding, headers: HTTPHeaders?) -> SignalProducer<T, NSError> where T : Decodable {
        guard let url = URL(string: address, relativeTo: baseURL) else { return .empty }
        URLCache.shared.removeAllCachedResponses()
        let decoder = JSONDecoder()
        return SignalProducer { [unowned self] observer, _ in
            self.session.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers, interceptor: nil)
                .responseDecodable(of: T.self, decoder: decoder) { response in
                    switch response.result {
                    case let .success(value):
                        observer.send(value: value)
                        observer.sendCompleted()
                    case .failure:
                        let nsError = NSError(domain: "", code: response.response?.statusCode ?? 0, userInfo: nil)
                        observer.send(error: nsError)
                    }
            }
        }
    }
    
    public func requesta<T>(_ address: String, method: HTTPMethod, parameters: Parameters?, encoding: ParameterEncoding, headers: HTTPHeaders?) -> SignalProducer<[T], NSError> where T : Decodable {
        guard let url = URL(string: address, relativeTo: baseURL) else { return .empty }
        URLCache.shared.removeAllCachedResponses()
        let decoder = JSONDecoder()
        return SignalProducer { [unowned self] observer, _ in
            self.session.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers, interceptor: nil)
                .responseDecodable(of: [T].self, decoder: decoder) { response in
                    switch response.result {
                    case let .success(value):
                        observer.send(value: value)
                        observer.sendCompleted()
                    case .failure:
                        let nsError = NSError(domain: "", code: response.response?.statusCode ?? 0, userInfo: nil)
                        observer.send(error: nsError)
                    }
            }
        }
    }
    
    public func request(_ address: String, method: HTTPMethod, parameters: Parameters?, encoding: ParameterEncoding, headers: HTTPHeaders?) -> SignalProducer<Void, NSError> {
        guard let url = URL(string: address, relativeTo: baseURL) else { return .empty }
        URLCache.shared.removeAllCachedResponses()
        return SignalProducer { [unowned self] observer, _ in
            self.session.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers)
                .response { response in
                    switch response.result {
                    case .success:
                        observer.send(value: ())
                        observer.sendCompleted()
                    case .failure:
                        let nsError = NSError(domain: "", code: response.response?.statusCode ?? 0, userInfo: nil)
                        observer.send(error: nsError)
                    }
            }
        }
    }
    
    public var baseURL: URL
    typealias Dependencies = HasNoDependency
    private let session: Session
    
    init(dependencies: Dependencies) {
        baseURL = URL(string: "http://private-9aad-note10.apiary-mock.com")!
        let configuration = URLSessionConfiguration.default
        session = Session(configuration: configuration)
    }
}
