import Foundation

final class NoteUpdateResponse: Decodable {
    let title: String
    
    private enum CodingKeys: String, CodingKey {
        case title
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.title = try container.decode(String.self, forKey: .title)
    }
}

final class Note: Decodable {
    let id: IdentifierType
    var title: String
    
    private enum CodingKeys: String, CodingKey {
        case id
        case title
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(IdentifierType.self, forKey: .id)
        self.title = try container.decode(String.self, forKey: .title)
    }
    public init(id: IdentifierType, title: String) {
        self.id = id
        self.title = title
    }
}

extension Note: Equatable {
    public static func ==(lhs: Note, rhs: Note) -> Bool {
        return lhs.id == rhs.id
    }
}
