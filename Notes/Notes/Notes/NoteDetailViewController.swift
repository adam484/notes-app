import Foundation
import UIKit
import ReactiveCocoa
import ReactiveSwift

protocol NoteDetailViewControllerFlowDelegate: class {
    func userDidDeleteNote()
    func userDidUpdateNote()
    func userDidCreateNote(note: Note)
}

final class NoteDetailViewController: BaseScrollViewController {
    private weak var textView: UITextView!
    private weak var saveButton: UIButton!
    private weak var deleteButton: UIButton!
    private weak var createButton: UIButton!
    weak var delegate: NoteDetailViewControllerFlowDelegate?
    
    private let viewModel: NoteDetailViewModeling
    init(viewModel: NoteDetailViewModeling) {
        self.viewModel = viewModel
        super.init()
    }
    
    public required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        let textView = UITextView()
        self.textView = textView
        textView.isScrollEnabled = false
        textView.font = UIFont.systemFont(ofSize: 16)
        scrollContentView.addSubview(textView)
        textView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(16)
            make.height.equalTo(view.snp.height)
        }
        
        let saveButton = UIButton(type: .roundedRect)
        saveButton.setTitle("Save", for: .normal)
        saveButton.addTarget(self, action: #selector(saveButtonTapped), for: .touchUpInside)
        self.saveButton = saveButton
        
        let deleteButton = UIButton(type: .roundedRect)
        deleteButton.setTitle("Delete", for: .normal)
        deleteButton.addTarget(self, action: #selector(deleteButtonTapped), for: .touchUpInside)
        self.deleteButton = deleteButton
        
        let createButton = UIButton(type: .roundedRect)
        createButton.setTitle("Create", for: .normal)
        createButton.addTarget(self, action: #selector(createButtonTapped), for: .touchUpInside)
        self.createButton = createButton
        
        let hStack = UIStackView(arrangedSubviews: [saveButton, createButton, deleteButton])
        hStack.spacing = 15
        
        let rightItem = UIBarButtonItem(customView: hStack)
        rightItem.style = .plain
        navigationItem.rightBarButtonItem = rightItem
    }
    
    @objc func saveButtonTapped() {
        viewModel.actions.updateNote.apply().start()
    }
    
    @objc func deleteButtonTapped() {
        viewModel.actions.deleteNote.apply().start()
    }
    
    @objc func createButtonTapped() {
        viewModel.actions.createNote.apply().start()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.text = viewModel.title.value
        setupBindings()
        
        viewModel.actions.createNote.values
            .observe(on: QueueScheduler.main)
            .take(duringLifetimeOf: self)
            .observeValues { [weak self] newNote in
                self?.delegate?.userDidCreateNote(note: newNote)
        }
        
        viewModel.actions.updateNote.values
                   .observe(on: QueueScheduler.main)
                   .take(duringLifetimeOf: self)
                   .observeValues { [weak self] updatedNoteTitle in
                    self?.delegate?.userDidUpdateNote()
        }
        
        viewModel.actions.deleteNote.completed
                   .observe(on: QueueScheduler.main)
                   .take(duringLifetimeOf: self)
                   .observeValues { [weak self] _ in
                    self?.delegate?.userDidDeleteNote()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        textView.becomeFirstResponder()
    }
    
    private func setupBindings() {
        saveButton.reactive.isHidden <~ Property.combineLatest(viewModel.title, viewModel.note).map { $0 == nil || $1 == nil }
        createButton.reactive.isHidden <~ Property.combineLatest(viewModel.title, viewModel.note).map { !($0 ?? "").isEmpty && $1 == nil }.negate()
        deleteButton.reactive.isHidden <~ viewModel.note.map { $0 == nil }
        viewModel.title <~ textView.reactive.continuousTextValues
        reactive.isLoading <~ viewModel.actions.createNote.isExecuting
            .or(viewModel.actions.deleteNote.isExecuting)
            .or(viewModel.actions.updateNote.isExecuting)
        reactive.errors() <~ Signal.merge(viewModel.actions.createNote.errors,
                                viewModel.actions.deleteNote.errors,
                                viewModel.actions.updateNote.errors)
    }
}
