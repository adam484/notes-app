import Foundation
import ReactiveSwift

protocol NoteDetailViewModelingActions {
    var deleteNote: Action<Void, Void, NSError> { get }
    var createNote: Action<Void, Note, NSError> { get }
    var updateNote: Action<Void, NoteUpdateResponse, NSError> { get }
}

protocol NoteDetailViewModeling {
    var actions: NoteDetailViewModelingActions { get }
    var note: MutableProperty<Note?> { get }
    var title: MutableProperty<String?> { get }
}

final class NoteDetailViewModel: NoteDetailViewModeling, NoteDetailViewModelingActions {
    typealias Dependencies = HasNotesAPI
    var actions: NoteDetailViewModelingActions { return self }
    private let notesAPI: NotesAPIServicing
    var note: MutableProperty<Note?>
    var title: MutableProperty<String?>
    
    init(dependencies: Dependencies, note: Note? = nil) {
        self.notesAPI = dependencies.notesAPI
        self.note = MutableProperty(note)
        self.title = MutableProperty(note?.title)
    }
    
    lazy var deleteNote: Action<Void, Void, NSError> = Action { [weak self] _ in
        guard let self = self, let id = self.note.value?.id else { return .empty }
        return self.notesAPI.deleteNote(id: id)
    }
    
    lazy var createNote: Action<Void, Note, NSError> = Action { [weak self] note in
        guard let self = self, let title = self.title.value else { return .empty }
        return self.notesAPI.createNote(title: title)
    }
    
    lazy var updateNote: Action<Void, NoteUpdateResponse, NSError> = Action { [weak self] note in
        guard let self = self, let note = self.note.value else { return .empty }
        return self.notesAPI.updateNote(note: note)
    }
}
