import Foundation
import UIKit
import ReactiveSwift

protocol NotesViewControllerFlowDelegate: class {
    func didSelect(note selectedNote: Note, in viewController: NotesViewController)
    func didTapCreateNewNote(in viewController: NotesViewController)
}

final class NotesViewController: BaseTableViewController {
    private let viewModel: NotesViewModeling
    weak var delegate: NotesViewControllerFlowDelegate?
    init(viewModel: NotesViewModeling) {
        self.viewModel = viewModel
        super.init()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.contentInsetAdjustmentBehavior = .never

        tableView.estimatedRowHeight = 205
        tableView.showsVerticalScrollIndicator = false
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.register(NoteCell.self, forCellReuseIdentifier: NoteCell.identifier)
        
        let plusButton = UIButton(type: .contactAdd)
        let rightItem = UIBarButtonItem(customView: plusButton)
        plusButton.addTarget(self, action: #selector(plusButtonTapped), for: .touchUpInside)
        self.navigationItem.title = "Notes"
        self.navigationItem.setRightBarButton(rightItem, animated: true)
        
        viewModel.actions.loadNotes.apply().start()
        refreshControl.addTarget(self, action: #selector(refreshControlActivated), for: .valueChanged)
        setupBindings()
    }
    
    private func setupBindings() {
        refreshControl.reactive.isRefreshing <~ viewModel.actions.loadNotes.isExecuting
        tableView.reactive.reloadData <~ Signal.merge(viewModel.actions.loadNotes.completed, viewModel.actions.updateNotes.completed).map { _ in }
        
        reactive.errors() <~ viewModel.actions.loadNotes.errors
    }
    
    @objc func refreshControlActivated() {
        viewModel.actions.loadNotes.apply().start()
    }
    
    @objc func plusButtonTapped() {
        delegate?.didTapCreateNewNote(in: self)
    }
    
    public func fetchData() {
        viewModel.actions.loadNotes.apply().start()
    }
    
    public func updateNotes(note: Note) {
        viewModel.actions.updateNotes.apply(note).start()
    }
    
    public func updateData() {
        tableView.reloadData()
    }
}

extension NotesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.notes.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let note = viewModel.notes.value[safe: indexPath.row],
            let cell = tableView.dequeueReusableCell(withIdentifier: NoteCell.identifier, for: indexPath) as? NoteCell else { return UITableViewCell() }
        cell.note = note
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let note = viewModel.notes.value[safe: indexPath.row] else { return }
        delegate?.didSelect(note: note, in: self)
    }
}
