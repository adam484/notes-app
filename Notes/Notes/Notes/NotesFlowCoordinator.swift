import Foundation
import UIKit

final class NotesFlowCoordinator: BaseFlowCoordinator {
    typealias Dependencies = HasNotesAPI
    private let dependencies: Dependencies
    
    public init(dependencies: Dependencies) {
        self.dependencies = dependencies
        super.init()
    }
    
    public override func start() {
        super.start()
        let notesVM = NotesViewModel(dependencies: dependencies)
        let notesVC = NotesViewController(viewModel: notesVM)
        notesVC.delegate = self
        let navigationController = BaseNavigationController(rootViewController: notesVC)
        self.navigationController = navigationController
        
        if let rootViewController = rootViewController as? RootViewControllerProtocol {
            rootViewController.set(rootViewController: navigationController, animated: false)
        }
    }
}

extension NotesFlowCoordinator: NotesViewControllerFlowDelegate {
    func didSelect(note selectedNote: Note, in viewController: NotesViewController) {
        let detailVM = NoteDetailViewModel(dependencies: dependencies, note: selectedNote)
        let detailVC = NoteDetailViewController(viewModel: detailVM)
        detailVC.delegate = self
        navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func didTapCreateNewNote(in viewController: NotesViewController) {
        let detailVM = NoteDetailViewModel(dependencies: dependencies)
        let detailVC = NoteDetailViewController(viewModel: detailVM)
        detailVC.delegate = self
        navigationController?.pushViewController(detailVC, animated: true)
    }
}

extension NotesFlowCoordinator: NoteDetailViewControllerFlowDelegate {
    func userDidDeleteNote() {
        navigationController?.popViewController(animated: true)
        if let notesViewController = navigationController?.viewControllers.last as? NotesViewController {
            notesViewController.fetchData()
        }
    }
    
    func userDidUpdateNote() {
        navigationController?.popViewController(animated: true)
        if let notesViewController = navigationController?.viewControllers.last as? NotesViewController {
            notesViewController.updateData()
        }
    }
    
    func userDidCreateNote(note: Note) {
        navigationController?.popViewController(animated: true)
        if let notesViewController = navigationController?.viewControllers.last as? NotesViewController {
            notesViewController.updateNotes(note: note)
        }
    }
}
