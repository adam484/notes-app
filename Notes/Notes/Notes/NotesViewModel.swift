import Foundation
import ReactiveSwift

protocol NotesViewModelingActions {
    var loadNotes: Action<Void, [Note], NSError> { get }
    var updateNotes: Action<Note, [Note], Never> { get }
}

protocol NotesViewModeling {
    var notes: Property<[Note]> { get }
    var actions: NotesViewModelingActions { get }
}

final class NotesViewModel: NotesViewModeling, NotesViewModelingActions {
    
    public typealias Dependencies = HasNotesAPI
    private let notesAPI: NotesAPIServicing
    
    var actions: NotesViewModelingActions { return self }
    lazy var loadNotes: Action<Void, [Note], NSError> = Action { [weak self] _ in
        guard let self = self else { return .empty }
        return self.notesAPI.notes()
    }
    
    /// Used to update notes array  in case of creation or update of existing note
    lazy var updateNotes: Action<Note, [Note] , Never> = Action { note in
        return SignalProducer { [weak self] observer, _ in
            guard let self = self else {
                observer.sendCompleted()
                return
            }
            var notes = self.notes.value
            if let index = notes.firstIndex(where: { $0 == note }) {
                notes[index] = note
            } else {
                notes.append(note)
            }
            observer.send(value: notes)
            observer.sendCompleted()
        }
    }

    lazy var notes: Property<[Note]> = Property(initial: [], then: Signal.merge(self.updateNotes.values, self.loadNotes.values))
    
    init(dependencies: Dependencies) {
        self.notesAPI = dependencies.notesAPI
    }
}
