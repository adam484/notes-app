import Foundation
import UIKit

public final class RootFlowCoordinator: BaseFlowCoordinator {
    typealias RootDependencies = HasAllDependencies
    let window: UIWindow
    let dependencies: RootDependencies
    
    init(dependencies: RootDependencies, window: UIWindow) {
        self.dependencies = dependencies
        self.window = window
        super.init()
        let rootViewController = RootViewController()
        window.rootViewController = rootViewController
        self.rootViewController = rootViewController
    }
    
    public override func start() {
        window.makeKeyAndVisible()
        self.startNotesFlow()
    }
    
    private func startNotesFlow() {
         if getChild(NotesFlowCoordinator.self) == nil {
             let notesFlow = NotesFlowCoordinator(dependencies: dependencies)
             notesFlow.rootViewController = rootViewController
             notesFlow.parent = self
             addChild(notesFlow)
             notesFlow.start()
         }
     }
}
