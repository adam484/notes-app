import Foundation
import UIKit
import SnapKit

public protocol RootViewControllerProtocol {
    var currentController: UIViewController? { get set }
    func set(rootViewController viewController: UIViewController, animated _: Bool)
}

public final class RootViewController: UIViewController & RootViewControllerProtocol {
    public weak var currentController: UIViewController?

    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public init() {
        super.init(nibName: nil, bundle: nil)
    }

    // MARK: Public interface

    public func set(rootViewController viewController: UIViewController, animated _: Bool = false) {
        currentController?.willMove(toParent: nil)
        currentController?.view.removeFromSuperview()
        currentController?.removeFromParent()
        currentController?.didMove(toParent: nil)

        viewController.willMove(toParent: self)
        addChild(viewController)
        view.addSubview(viewController.view)
        viewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        viewController.didMove(toParent: self)
        currentController = viewController
    }

    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return currentController?.preferredStatusBarStyle ?? .lightContent
    }
}
