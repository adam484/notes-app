import Foundation
import ReactiveSwift

extension Reactive where Base: UIResponder {
    public func errors<Error>() -> BindingTarget<Error> where Error: NSError {
        return makeBindingTarget(on: QueueScheduler.main) { (base, value) in
            base.displayError(value)
        }
    }
}

extension Reactive where Base: BaseViewController {
    var isLoading: BindingTarget<Bool> {
        return makeBindingTarget { $0.isLoading = $1 }
    }
}

