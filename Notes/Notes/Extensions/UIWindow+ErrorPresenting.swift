import Foundation
import UIKit
import ReactiveSwift

extension UIWindow: ErrorPresenting {
    public func presentError(_ e: NSError) -> Bool {
        guard let window = UIApplication.shared.keyWindow else { return false }
        let alert = AlertController.info(title: "Error", message: e.localizedDescription)
        window.rootViewController?.frontmostController.present(alert, animated: true)
        return true
    }
}

public protocol ErrorPresenting {
    func presentError(_ e: NSError) -> Bool
}

public extension UIResponder {
    func displayError(_ e: NSError) {
        if (self as? ErrorPresenting)?.presentError(e) == true {
            return
        } else {
            next?.displayError(e)
        }
    }
}
