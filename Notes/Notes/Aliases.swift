import Alamofire

public typealias URLConvertible = Alamofire.URLConvertible
public typealias HTTPHeaders = Alamofire.HTTPHeaders
public typealias URLEncoding = Alamofire.URLEncoding
public typealias JSONEncoding = Alamofire.JSONEncoding
public typealias Parameters = Alamofire.Parameters

public typealias IdentifierType = Int64
