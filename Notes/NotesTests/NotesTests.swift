import XCTest
@testable import Notes
import ReactiveSwift

class NotesTests: XCTestCase {
    class Dependencies: NotesViewModel.Dependencies & HasNetwork & HasNoDependency{
        lazy var notesAPI: NotesAPIServicing = NotesAPI(dependencies: self)
        lazy var network: Networking = Network(dependencies: self)
    }
    
    private var dependencies: Dependencies!
    private let initialNotes: [Note] = [
        Note(id: 1, title: "Note1"),
        Note(id: 2, title: "Note2")
    ]
    
    override func setUp() {
        self.dependencies = Dependencies()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    func testUpdateNote() {
        let notesVM = NotesViewModel(dependencies: dependencies)
        notesVM.notes = Property(value: initialNotes)
        let updatedNote = Note(id: 1, title: "Note1 with updated title")
        
        let updatedNotes: [Note] = [
            updatedNote,
            Note(id: 2, title: "Note2")
        ]
        
        notesVM.updateNotes.apply(updatedNote).on(value: { [unowned self] notes in
            XCTAssertFalse(self.equal(lhs: self.initialNotes, rhs: notes))
            XCTAssertTrue(self.equal(lhs: notes, rhs: updatedNotes))
        }).start()
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    private func equal(lhs: [Note], rhs: [Note]) -> Bool {
           for (index, note) in lhs.enumerated() {
               if rhs[safe: index]?.title != note.title || rhs[safe: index]?.id != note.id {
                   return false
               }
           }
           return true
    }
}
